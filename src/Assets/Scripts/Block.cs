﻿using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

public sealed class Block : MonoBehaviour
{
    private readonly static Color COLOR_HIGHLIGHT_TRUE = new Color32(200, 150, 200, 255);
    private readonly static Color COLOR_HIGHLIGHT_FALSE = new Color32(50, 50, 50, 255);

    private readonly static Color COLOR_REPLACEMENT_FROM = new Color32(229, 174, 255, 255);
    private readonly static Color COLOR_REPLACEMENT_TO = new Color32(115, 94, 125, 255);

    [SerializeField]
    private SpriteRenderer body = null;

    [SerializeField]
    private SpriteRenderer outline = null;

    [SerializeField]
    private bool canBeCopied = true;

    [SerializeField]
    private bool canBeReplaced = true;

    [SerializeField]
    private int replacementCharges = 3;

    [SerializeField]
    private float replacementTime = 6.0f;

    [SerializeField]
    private Behaviour[] replacementDisabledBehaviours = null;

    [SerializeField]
    private GameObject[] replacementDisabledObjects = null;

    [SerializeField]
    [HideInInspector]
    private TransformEffect fadeEffect = null;

    [SerializeField]
    [HideInInspector]
    private bool isHighlighted = false;

    public bool IsHighlighted
    {
        get
        {
            return this.isHighlighted;
        }

        set
        {
            if (this.isHighlighted != value)
            {
                this.isHighlighted = value;

                int orderChange = 10;

                if (!this.isHighlighted)
                {
                    orderChange = -orderChange;
                }

                this.body.sortingOrder += orderChange;

                if (this.outline != null)
                {
                    this.outline.sortingOrder += orderChange;

                    this.outline.color = this.isHighlighted ? Block.COLOR_HIGHLIGHT_TRUE : Block.COLOR_HIGHLIGHT_FALSE;
                }
            }
        }
    }

    public bool CanBeCopied
    {
        get
        {
            return this.canBeCopied;
        }
    }

    public bool CanBeReplaced
    {
        get
        {
            return this.canBeReplaced;
        }
    }

    public int ReplacementCharges
    {
        get
        {
            return this.replacementCharges;
        }
    }

    public Block Original { get; private set; }

    public bool IsOriginal
    {
        get
        {
            return this.Original == null;
        }
    }

    public Block Replacement { get; private set; }

    public bool IsDestroying { get; private set; }

    public bool ReplaceWith(Block replacementPrefab)
    {
        if (this.Replacement == null && !this.IsDestroying)
        {
            this.Replacement = (Block)Component.Instantiate(replacementPrefab, this.transform.position, this.transform.rotation);
            this.Replacement.Original = this;

            ColorEffect replacementEffect = this.Replacement.gameObject.AddComponent<ColorEffect>();
            replacementEffect.TimeWrap = WrapType.Clamp;
            replacementEffect.Duration = this.replacementTime;
            replacementEffect.AffectChildren = true;
            replacementEffect.FromColor = Block.COLOR_REPLACEMENT_FROM;
            replacementEffect.ToColor = Block.COLOR_REPLACEMENT_TO;

            replacementEffect.Events.Register(EffectEvents.Trigger.DurationReached, () => this.Replacement.RestoreOriginal());

            this.fadeEffect.PlayForward();

            this.Replacement.fadeEffect.SimulatedTime = this.Replacement.fadeEffect.Duration;

            for (int i = 0; i < this.replacementDisabledBehaviours.Length; ++i)
            {
                this.replacementDisabledBehaviours[i].enabled = false;
            }

            for (int i = 0; i < this.replacementDisabledObjects.Length; ++i)
            {
                this.replacementDisabledObjects[i].SetActive(false);
            }

            return true;
        }

        return false;
    }

    public void RestoreOriginal()
    {
        if (!this.IsOriginal)
        {
            for (int i = 0; i < this.Original.replacementDisabledBehaviours.Length; ++i)
            {
                this.Original.replacementDisabledBehaviours[i].enabled = true;
            }

            for (int i = 0; i < this.Original.replacementDisabledObjects.Length; ++i)
            {
                this.Original.replacementDisabledObjects[i].SetActive(true);
            }

            this.Original.fadeEffect.PlayBackward();
            this.Original.Replacement = null;
            this.Original = null;

            this.Destroy();
        }
    }

    public void Destroy()
    {
        if (!this.IsDestroying)
        {
            this.fadeEffect.PlayForward();

            GameObject.Destroy(this.gameObject, Mathf.Abs(this.fadeEffect.Duration / this.fadeEffect.Speed));

            this.IsDestroying = true;
        }
    }

    private void Awake()
    {
        if (this.fadeEffect == null)
        {
            this.fadeEffect = this.gameObject.AddComponent<TransformEffect>();
            this.fadeEffect.TimeWrap = WrapType.Clamp;
            this.fadeEffect.Duration = 0.25f;
            this.fadeEffect.ScaleExtent = new Vector3(-1.0f, -1.0f, -1.0f);

            this.fadeEffect.PlayBackward();
        }
    }
}
