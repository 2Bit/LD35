﻿using UnityEngine;

public sealed class DieOnCollision : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particleChild = null;

    [SerializeField]
    private AudioSource audioChild = null;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerMovement otherPlayerMovement = collision.gameObject.GetComponentInChildren<PlayerMovement>();
        if (otherPlayerMovement != null)
        {
            this.particleChild.Play();
            this.particleChild.transform.SetParent(null);
            GameObject.Destroy(this.particleChild.gameObject, 10.0f);

            this.audioChild.Play();
            this.audioChild.transform.SetParent(null);
            GameObject.Destroy(this.audioChild.gameObject, 10.0f);
            
            GameObject.Destroy(this.gameObject);
        }
    }
}
