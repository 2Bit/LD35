﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

using Slerpy;
using Slerpy.Unity3D;

public class HUD : MonoBehaviour
{
	private GameObject player;
	private Gun gun;
	private Canvas canvas;
	private Image blockSprite;
	private Text chargeCount;
	
	public Effect expandIn;
	public Effect changeBlock;
	
	private Block prevBlock;
	private int prevCharges;
	
	void Start()
	{
		this.player = GameObject.Find("Player");
		this.canvas = gameObject.GetComponent<Canvas>();
		this.gun = this.player.GetComponentInChildren<Gun>();
		this.blockSprite = gameObject.GetComponentInChildren<Image>();
		this.chargeCount = gameObject.GetComponentInChildren<Text>();
	}
	
	void Update()
	{
		Block block = this.gun.ReplacementBlock;
		int charges = this.gun.ReplacementCharges;
		this.chargeCount.text = charges.ToString();
		if (block != null && charges > 0)
		{
			if (block != this.prevBlock || charges != this.prevCharges)
			{
				this.changeBlock.Rewind();
				this.changeBlock.PlayForward();
			}
			
			this.prevBlock = block;
			this.prevCharges = charges;
			
			this.expandIn.PlayForward();
			this.blockSprite.sprite = block.gameObject.GetComponentInChildren<SpriteRenderer>().sprite;
		}
		else {
			this.expandIn.PlayBackward();
		}
	}
	
	void LateUpdate()
	{
		//Position the HUD relative to the player
		gameObject.GetComponent<RectTransform>().position = this.player.transform.position + new Vector3(0, 1.0f, 0);
	}
}
