﻿using UnityEngine;

public sealed class Init : MonoBehaviour
{
    [SerializeField]
    private float loadDelay = 3.0f;

    private void Awake()
    {
        Object.DontDestroyOnLoad(this.gameObject);
    }

    private void Update()
    {
        if (this.loadDelay >= 0.0f)
        {
            if (Input.anyKey)
            {
                this.loadDelay = 0.0f;
            }

            this.loadDelay -= Time.deltaTime;

            if (this.loadDelay <= 0.0f)
            {
                Application.LoadLevel("Level_Tutorial");
            }
        }
    }
}
