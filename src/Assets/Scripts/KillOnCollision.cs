﻿using UnityEngine;
using System.Collections;

public class KillOnCollision : MonoBehaviour {
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerMovement otherPlayerMovement = collision.gameObject.GetComponentInChildren<PlayerMovement>();
        if (otherPlayerMovement != null)
        {
            otherPlayerMovement.Die();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        PlayerMovement otherPlayerMovement = other.gameObject.GetComponentInChildren<PlayerMovement>();
        if (otherPlayerMovement != null)
        {
            otherPlayerMovement.Die();
        }
    }
}
