﻿using UnityEngine;
using System.Collections;
using Prime31;

using Slerpy;
using Slerpy.Unity3D;


public class PlayerMovement : MonoBehaviour
{
	// movement config
	public float gravity = -25f;
	public float runSpeed = 8f;
	public float groundDamping = 20f; // how fast do we change direction? higher means faster
	public float inAirDamping = 5f;
	public float jumpHeight = 3f;

    [SerializeField]
    private ParticleSystem deathParticles = null;

    [SerializeField]
    private SpriteRenderer bodyRenderer = null;

    [SerializeField]
    private AudioSource deathSound = null;

    [SerializeField]
    private AudioSource jumpSound = null;

    [SerializeField]
    private ParticleSystem[] jumpLandParticles = null;

    private float jumpLandPlayTime = 0.0f;

	[HideInInspector]
	private float normalizedHorizontalSpeed = 0;

	private CharacterController2D _controller;

    [SerializeField]
	public Animator animator = null;
	
	private RaycastHit2D _lastControllerColliderHit;
	private Vector3 _velocity;

    public ParticleSystem DeathParticles
    {
        get
        {
            return this.deathParticles;
        }
    }

    public void Die()
    {
        GameObject.FindObjectOfType<Level>().Die();

        this.deathParticles.Play();

        this.bodyRenderer.transform.SetParent(null);
        
        TransformEffect deathRiseEffect = this.bodyRenderer.gameObject.AddComponent<TransformEffect>();
        deathRiseEffect.TimeWrap = WrapType.Clamp;
        deathRiseEffect.Duration = 3.0f;
        deathRiseEffect.PositionExtent = new Vector3(0.0f, 2.0f, 0.0f);

        TransformEffect deathSpinEffect = this.bodyRenderer.gameObject.AddComponent<TransformEffect>();
        deathSpinEffect.SetWeights(WeightType.Eager);
        deathSpinEffect.TimeWrap = WrapType.Clamp;
        deathSpinEffect.Duration = 5.0f;
        deathSpinEffect.RotationExtent = new Vector3(0.0f, 0.0f, -180.0f);

        ColorEffect deathFadeEffect = this.bodyRenderer.gameObject.AddComponent<ColorEffect>();
        deathFadeEffect.SetWeights(WeightType.Eager);
        deathFadeEffect.TimeWrap = WrapType.Clamp;
        deathFadeEffect.Duration = 1.0f;
        deathFadeEffect.AffectChildren = true;
        deathFadeEffect.FromColor = Color.white;
        deathFadeEffect.ToColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);

        this.deathSound.Play();

        Component.Destroy(this.bodyRenderer.gameObject.GetComponent<Animator>());

        Component.Destroy(this);
    }

	void Awake()
	{
		_controller = GetComponent<CharacterController2D>();

		// listen to some events for illustration purposes
		//_controller.onControllerCollidedEvent += onControllerCollider;
		//_controller.onTriggerEnterEvent += onTriggerEnterEvent;
		//_controller.onTriggerExitEvent += onTriggerExitEvent;
	}


	#region Event Listeners

	void onControllerCollider( RaycastHit2D hit )
	{
		// bail out on plain old ground hits cause they arent very interesting
		if( hit.normal.y == 1f )
			return;

		// logs any collider hits if uncommented. it gets noisy so it is commented out for the demo
		//Debug.Log( "flags: " + _controller.collisionState + ", hit.normal: " + hit.normal );
	}

	#endregion


	// the Update loop contains a very simple example of moving the character around and controlling the animation
	void Update()
	{
		if( _controller.isGrounded )
        {
            if (this.jumpLandPlayTime >= 0.0f)
            {
                this.jumpLandPlayTime -= Time.deltaTime;
            }

			_velocity.y = 0;
        }

        for (int i = 0; i < this.jumpLandParticles.Length; ++i)
        {
            this.jumpLandParticles[i].enableEmission = _controller.isGrounded && this.jumpLandPlayTime >= 0.0f;
        }

		if( Input.GetAxisRaw("Horizontal") > 0.0f )
		{
			normalizedHorizontalSpeed = 1;

	        transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

			if( _controller.isGrounded )
				this.animator.Play("Player_Walk");
		}
		else if( Input.GetAxisRaw("Horizontal") < 0.0f )
		{
			normalizedHorizontalSpeed = -1;

		    transform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);

			if( _controller.isGrounded )
				this.animator.Play("Player_Walk");
		}
		else
		{
			normalizedHorizontalSpeed = 0;

			if( _controller.isGrounded )
				this.animator.Play("Player_Idle");
		}


		// we can only jump whilst grounded
		if( _controller.isGrounded && Input.GetButtonDown("Jump") )
		{
			_velocity.y = Mathf.Sqrt( 2f * jumpHeight * -gravity );
			this.animator.Play("Player_Jump");

            this.jumpLandPlayTime = 0.15f;
		}


		// apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
		var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
		_velocity.x = Mathf.Lerp( _velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor );

		// apply gravity before moving
		_velocity.y += gravity * Time.deltaTime;

		// if holding down bump up our movement amount and turn off one way platform detection for a frame.
		// this lets uf jump down through one way platforms
		if( _controller.isGrounded && Input.GetKey( KeyCode.DownArrow ) )
		{
			_velocity.y *= 3f;
			_controller.ignoreOneWayPlatformsThisFrame = true;
		}

		_controller.move( _velocity * Time.deltaTime );

		// grab our current _velocity to use as a base for all calculations
		_velocity = _controller.velocity;
	}

}
