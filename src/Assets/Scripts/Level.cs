﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    [SerializeField]
    private string nextLevel;

    public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.

    private bool sceneStarting = true;      // Whether or not the scene is still fading in.
    private bool sceneEnding = false;      // Whether or not the scene is still fading in.
    private bool dead = false;      // Whether or not the scene is still fading in.

    private Material planeMaterial;

    void Awake()
    {
        // Set the texture so that it is the the size of the screen and covers it.
        this.planeMaterial = gameObject.GetComponent<MeshRenderer>().material;
    }


    void Update()
    {
        // If the scene is starting...
        if (this.sceneStarting)
        {
            // ... call the StartScene function.
            this.StartScene();
        }

        if (this.sceneEnding)
        {
            this.ProcessEndScene();
        }

    }


    void FadeToClear()
    {
        // Lerp the colour of the texture between itself and transparent.
        this.planeMaterial.color = Color.Lerp(this.planeMaterial.color, Color.clear, fadeSpeed * Time.deltaTime);
    }


    void FadeToBlack()
    {
        // Lerp the colour of the texture between itself and black.
        this.planeMaterial.color = Color.Lerp(this.planeMaterial.color, Color.black, fadeSpeed * Time.deltaTime);
    }


    void StartScene()
    {
        // Fade the texture to clear.
        FadeToClear();

        // If the texture is almost clear...
        if (this.planeMaterial.color.a <= 0.05f)
        {
            // ... set the colour to clear and disable the GUITexture.
            this.planeMaterial.color = Color.clear;
            this.sceneStarting = false;
        }
    }

    public void Die()
    {
        this.dead = true;
        this.EndScene();
    }

    public void EndScene()
    {
        this.sceneStarting = false;
        this.sceneEnding = true;
    }

    public void ProcessEndScene()
    {
        // Start fading towards black.
        FadeToBlack();

        // If the screen is almost black...
        if (this.planeMaterial.color.a >= 0.95f) {
            // ... reload the level.

            this.sceneEnding = false;

            if (this.dead)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            } else
            {
                if (this.nextLevel == "")
                {
                    Application.Quit();
                }

                SceneManager.LoadScene(this.nextLevel);
            }
        }
    }
}