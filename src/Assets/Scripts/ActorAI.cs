﻿using UnityEngine;
using System.Collections;
using Prime31;

public class ActorAI : MonoBehaviour {

    private int horizontal = 1;

    private float speed = 1f;

    protected bool jump = false;

    // movement config
    public float gravity = -25f;
    public float runSpeed = 8f;
    public float groundDamping = 20f; // how fast do we change direction? higher means faster
    public float inAirDamping = 5f;
    public float jumpHeight = 3f;

    [HideInInspector]
    private float normalizedHorizontalSpeed = 0;

    private CharacterController2D _controller;

    [SerializeField]
    private Animator _animator;

    private RaycastHit2D _lastControllerColliderHit;
    private Vector3 _velocity;

    [SerializeField]
    private SpriteRenderer sRenderer;

    void Awake()
    {
        _controller = GetComponent<CharacterController2D>();

        // listen to some events for illustration purposes
        //_controller.onControllerCollidedEvent += onControllerCollider;
        //_controller.onTriggerEnterEvent += onTriggerEnterEvent;
        //_controller.onTriggerExitEvent += onTriggerExitEvent;
    }

    protected void Turn()
    {
        this.horizontal *= -1;
    }

    protected void Jump()
    {
        this.jump = true;
    }

    protected bool WalkBlocked()
    {
        int layerMask = ~((1 << LayerMask.NameToLayer("Player")) | (1 << LayerMask.NameToLayer("Player Ignore")));

        Vector2 forwardOffset = new Vector2(this.transform.position.x + horizontal * (_controller.boxCollider.bounds.extents.x + 0.05f), this.transform.position.y);
        var hit = Physics2D.Linecast(forwardOffset, forwardOffset + new Vector2(0f, -0.3f), layerMask);
        if (hit.collider == null)
        {
            return true;
        }

        hit = Physics2D.Linecast(forwardOffset, forwardOffset + new Vector2(0f, 0.1f), layerMask);

        if(hit.collider != null)
        {
            return true;
        }

        return false;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (this.WalkBlocked())
        {
            this.Turn();
        }

        this.ProcessMove();
	}

    void ProcessMove()
    {
        if (_controller.isGrounded)
            _velocity.y = 0;

        if (this.horizontal == 1)
        {
            normalizedHorizontalSpeed = 1;
            
            transform.localRotation = Quaternion.Euler(0.0f, 0.0f, 0.0f);

            if( _controller.isGrounded && _animator != null )
				_animator.Play( Animator.StringToHash( "Demon_Walk" ) );
        }
        else if (this.horizontal == -1)
        {
            normalizedHorizontalSpeed = -1;
            
            transform.localRotation = Quaternion.Euler(0.0f, 180.0f, 0.0f);

            if( _controller.isGrounded && _animator != null )
				_animator.Play( Animator.StringToHash( "Demon_Walk" ) );
        }
        else
        {
            normalizedHorizontalSpeed = 0;

            if( _controller.isGrounded && _animator != null )
				_animator.Play( Animator.StringToHash( "Demon_Idle" ) );
        }


        // we can only jump whilst grounded
        if (_controller.isGrounded && this.jump)
        {
            _velocity.y = Mathf.Sqrt(2f * jumpHeight * -gravity);
            //_animator.Play( Animator.StringToHash( "Jump" ) );
        }


        // apply horizontal speed smoothing it. dont really do this with Lerp. Use SmoothDamp or something that provides more control
        var smoothedMovementFactor = _controller.isGrounded ? groundDamping : inAirDamping; // how fast do we change direction?
        _velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * smoothedMovementFactor);

        // apply gravity before moving
        _velocity.y += gravity * Time.deltaTime;

        // if holding down bump up our movement amount and turn off one way platform detection for a frame.
        // this lets uf jump down through one way platforms
        if (_controller.isGrounded && Input.GetKey(KeyCode.DownArrow))
        {
            _velocity.y *= 3f;
            _controller.ignoreOneWayPlatformsThisFrame = true;
        }

        _controller.move(_velocity * Time.deltaTime);

        // grab our current _velocity to use as a base for all calculations
        _velocity = _controller.velocity;
    }
}
