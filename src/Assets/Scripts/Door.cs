﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {
    [SerializeField]
    private AudioSource useSound = null;

	void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerMovement>() != null)
        {
            this.UseDoor();

            Component.Destroy(other.GetComponent<PlayerMovement>());
        }
    }

    public void UseDoor()
    {
        GameObject.FindObjectOfType<Level>().EndScene();

        this.useSound.Play();
    }
}
