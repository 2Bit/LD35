﻿using UnityEngine;

public sealed class CycleSprite : MonoBehaviour
{
    private const float INTERVAL = 0.35f;

    [SerializeField]
    private Sprite[] sprites = null;

    public SpriteRenderer SpriteRenderer { get; private set; }

    private void Awake()
    {
        this.SpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if (this.SpriteRenderer != null)
        {
            this.SpriteRenderer.sprite = this.sprites[((int)(Time.time / CycleSprite.INTERVAL)) % this.sprites.Length];
        }
    }
}
