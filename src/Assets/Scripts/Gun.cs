﻿using UnityEngine;

using Slerpy;
using Slerpy.Unity3D;

public sealed class Gun : MonoBehaviour
{
    [SerializeField]
    private AudioSource soundSource = null;

    [SerializeField]
    private Vector3 fireOffset = Vector3.zero;

    [SerializeField]
    private Block replacementBlock = null;

    [SerializeField]
    private int replacementCharges = 0;
    
    private SpriteRenderer target = null;

    [SerializeField]
    private Sprite targetSprite = null;

    private Material targetLineMaterial = null;

    [SerializeField]
    private float targetDistanceMax = 5.0f;

    [SerializeField]
    private AudioClip copySound = null;

    [SerializeField]
    private AudioClip replacementSound = null;

    [SerializeField]
    private ParticleSystem replacementParticles = null;

    private Effect replacementClone = null;
    private Vector3 replacementCloneTargetPosition = Vector3.zero;
    private bool replacementCloneFromTarget = false;
    private Ghoster replacementCloneGhoster = null;
    private float replacementCloneTimeSinceGhost = 0.0f;

    private Block highlightedBlock = null;

    public Block ReplacementBlock
    {
        get
        {
            return this.replacementBlock;
        }
    }

    public int ReplacementCharges
    {
        get
        {
            return this.replacementCharges;
        }
    }

    public Vector3 FirePosition
    {
        get
        {
            return this.transform.position + this.transform.rotation * this.fireOffset;
        }
    }

    private void Start()
    {
        GameObject targetObject = new GameObject("Target");

        this.target = targetObject.AddComponent<SpriteRenderer>();
        this.target.sortingOrder = 1000;
        this.target.sprite = this.targetSprite;

        this.targetLineMaterial = new Material(Shader.Find("Unlit/Color"));
        this.targetLineMaterial.color = Color.black;
    }

    private void OnDestroy()
    {
        if (this.target != null)
        {
            GameObject.Destroy(this.target.gameObject);
        }
    }

    private void OnRenderObject()
    {
        this.targetLineMaterial.color = this.target.color;
        this.targetLineMaterial.SetPass(0);
        
        GL.Begin(GL.QUADS);

        Vector3 firePosition = this.FirePosition;

        Vector3 drawDisplacement = this.target.transform.position - firePosition;
        float drawDistance = drawDisplacement.magnitude - 0.2f;

        drawDisplacement.Normalize();

        Vector3 drawDisplacementCross = Vector3.Cross(drawDisplacement, Vector3.forward) * (0.5f * 0.02f);

        for (float i = 0.0f; i < drawDistance; i += 0.2f)
        {
            GL.Vertex(firePosition + drawDisplacement * i - drawDisplacementCross);
            GL.Vertex(firePosition + drawDisplacement * (i + 0.1f) - drawDisplacementCross);
            GL.Vertex(firePosition + drawDisplacement * (i + 0.1f) + drawDisplacementCross);
            GL.Vertex(firePosition + drawDisplacement * i + drawDisplacementCross);
        }

        GL.End();
    }

    private void Update()
    {
        Cursor.visible = Application.isEditor;

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        mousePosition.z = 0.0f;

        Collider2D hitCollider = Physics2D.OverlapPoint(mousePosition);

        Block hitBlock = null;

        if (hitCollider != null)
        {
            hitBlock = hitCollider.gameObject.GetComponent<Block>();
        }

        if (hitBlock != this.highlightedBlock)
        {
            if (this.highlightedBlock != null)
            {
                this.highlightedBlock.IsHighlighted = false;
            }

            this.highlightedBlock = hitBlock;

            if (this.highlightedBlock != null && (this.highlightedBlock.CanBeCopied || this.highlightedBlock.CanBeReplaced))
            {
                this.highlightedBlock.IsHighlighted = true;
            }
        }

        Vector3 mouseDisplacement = mousePosition - this.FirePosition;

        float mouseDistance = mouseDisplacement.magnitude;

        mouseDisplacement = mouseDisplacement.normalized * Mathf.Min(this.targetDistanceMax, mouseDistance);

        this.target.transform.position = this.FirePosition + mouseDisplacement;

        if (this.replacementClone == null)
        {
            if (hitBlock != null
                && (hitBlock.CanBeCopied || hitBlock.CanBeReplaced)
                && mouseDistance <= this.targetDistanceMax)
            {
                this.target.color = new Color32(229, 174, 255, 255);

                bool spawnReplacementClone = false;

                if (Input.GetButtonDown("Fire1")
                    && hitBlock.CanBeReplaced
                    && hitBlock.IsOriginal
                    && hitBlock != this.replacementBlock
                    && this.replacementBlock != null
                    && this.replacementCharges > 0)
                {
                    if (hitBlock.ReplaceWith(this.replacementBlock))
                    {
                        this.soundSource.PlayOneShot(this.replacementSound);

                        --this.replacementCharges;

                        spawnReplacementClone = true;

                        this.replacementCloneFromTarget = false;
                    }
                }

                if (Input.GetButtonDown("Fire2")
                    && hitBlock.CanBeCopied
                    && hitBlock.IsOriginal)
                {
                    this.soundSource.PlayOneShot(this.copySound);

                    this.replacementBlock = hitBlock;
                    this.replacementCharges = hitBlock.ReplacementCharges;

                    spawnReplacementClone = true;

                    this.replacementCloneFromTarget = true;
                }

                if (spawnReplacementClone)
                {
                    if (this.replacementCloneFromTarget)
                    {
                        this.replacementParticles.Play();
                        this.replacementParticles.transform.rotation = Quaternion.LookRotation(mousePosition - this.replacementParticles.transform.position);
                    }

                    GameObject copyCloneObject = GameObject.Instantiate(this.replacementBlock.gameObject);

                    foreach (Block block in copyCloneObject.GetComponentsInChildren<Block>())
                    {
                        Component.Destroy(block);
                    }

                    foreach (Collider2D collider in copyCloneObject.GetComponentsInChildren<Collider2D>())
                    {
                        Component.Destroy(collider);
                    }

                    this.replacementCloneGhoster = copyCloneObject.AddComponent<Ghoster>();
                    this.replacementCloneTimeSinceGhost = 0.0f;

                    TransformEffect replacementCloneFadeEffect = copyCloneObject.AddComponent<TransformEffect>();
                    replacementCloneFadeEffect.TimeWrap = WrapType.Clamp;
                    replacementCloneFadeEffect.SetWeights(WeightType.Heavy);
                    replacementCloneFadeEffect.Duration = this.replacementCloneFromTarget ? 0.75f : 0.2f;
                    replacementCloneFadeEffect.ScaleExtent = new Vector3(-0.8f, -0.8f, -0.8f);

                    this.replacementClone = replacementCloneFadeEffect;
                    this.replacementClone.Events.Register(EffectEvents.Trigger.DurationReached, () =>
                    {
                        foreach (Renderer renderer in copyCloneObject.GetComponentsInChildren<Renderer>())
                        {
                            Component.Destroy(renderer);
                        }

                        GameObject.Destroy(replacementCloneFadeEffect.gameObject, 5.0f);

                        this.replacementClone = null;
                    });

                    TransformEffect replacementCloneSpinEffect = copyCloneObject.AddComponent<TransformEffect>();
                    replacementCloneSpinEffect.TimeWrap = WrapType.Repeat;
                    replacementCloneSpinEffect.Duration = 0.3f;
                    replacementCloneSpinEffect.RotationExtent = new Vector3(0.0f, 0.0f, 360.0f);

                    if (this.replacementCloneFromTarget)
                    {
                        this.replacementCloneTargetPosition = this.replacementClone.transform.position;
                    }
                    else
                    {
                        this.replacementClone.transform.position = this.FirePosition;
                        this.replacementCloneTargetPosition = mousePosition;
                    }
                }
            }
            else
            {
                this.target.color = Color.red;
            }
        }
        else
        {
            Vector3 fromPosition = this.replacementCloneTargetPosition;
            Vector3 toPosition = this.FirePosition;

            if (!this.replacementCloneFromTarget)
            {
                Vector3 tempPosition = toPosition;
                toPosition = fromPosition;
                fromPosition = tempPosition;
            }

            this.replacementCloneTimeSinceGhost += Time.deltaTime;
            
            Ghoster ghost = this.replacementCloneGhoster.SpawnTemporary(0.2f);

            ColorEffect ghostColorEffect = ghost.gameObject.AddComponent<ColorEffect>();
            ghostColorEffect.TimeWrap = WrapType.Clamp;
            ghostColorEffect.Duration = 0.2f;
            ghostColorEffect.AffectChildren = true;
            ghostColorEffect.FromColor = Color.white;
            ghostColorEffect.ToColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);

            this.replacementCloneTimeSinceGhost -= 0.05f;

            this.replacementClone.transform.position = Vector3.Lerp(
                fromPosition,
                toPosition,
                this.replacementClone.CalculateWeight());
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;

        Gizmos.DrawSphere(this.FirePosition, 0.02f);
    }
}
