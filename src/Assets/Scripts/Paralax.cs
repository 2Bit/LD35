﻿using UnityEngine;

public sealed class Paralax : MonoBehaviour
{
    private void LateUpdate()
    {
        this.transform.localPosition = -this.transform.parent.position * 0.1f;
    }
}
